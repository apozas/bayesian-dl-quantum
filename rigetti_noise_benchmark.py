import scipy
import numpy as np
import os
import pyquil.api as api
import warnings
from tqdm import tqdm
from HHL_Rigetti import get_hhl_2x2, get_complete_hhl, repeat_until_success, \
                        fidelity, create_plot_fidelity, create_plot_repetitions
np.set_printoptions(precision=3, suppress=True)

###############################################################################
# Parameters ##################################################################
###############################################################################
n = 2
T = 4  # Bit precision
n_iterations = 100
noise_type = 1  # Choose the noise type to benchmark

###############################################################################
# Noise specification #########################################################
###############################################################################
noise_types = ['Gate Noise', 'Measurement Noise']
noise_levels = [np.linspace(0., 1.0, num=10), np.linspace(0., 1.0, num=10)]
###############################################################################
# Reference wavefunction ######################################################
###############################################################################
if not os.path.exists("output"):
    os.mkdir("output")
if not os.path.exists("figures"):
    os.mkdir("figures")
qvm = api.QVMConnection()
if n == 1:
    A = 0.5 * np.array([[3, 1], [1, 3]])
    b = [1, 0]
    r = 4
    qubits = list(range(4))
    hhl, flag = get_hhl_2x2(A, b, r, qubits)
else:
    A = np.diag([2**(-i) for i in range(2**n)])
    b = np.random.rand(2**n)
    b /= np.linalg.norm(b)
    Hr = np.random.randn(A.shape[0], A.shape[1])
    Hc = np.random.randn(A.shape[0], A.shape[1])
    U, _ = scipy.linalg.qr(Hr + 1j * Hc)
    A = U.dot(A.dot(U.conj().T))
    print(A)
    hhl, flag = get_complete_hhl(A, b, T)

reference_wavefunction, _ = repeat_until_success(qvm.wavefunction, hhl,
                                                 conditional_bit=flag)
print(reference_wavefunction)
reference_amplitudes = reference_wavefunction.amplitudes
noisy_repetitions = np.zeros((n_iterations, len(noise_levels[noise_type])))
fidelities = np.zeros((n_iterations, len(noise_levels[noise_type])))
for j, noise_level in enumerate(noise_levels[noise_type]):
    print('\n' + noise_types[noise_type], noise_level)
    for i in tqdm(range(n_iterations)):
        if noise_type == 0:
            gate_noise, meas_noise = [noise_level, 0., 0.], [0., 0., 0.]
        else:
            gate_noise, meas_noise = [0., 0., 0.], [noise_level, 0., 0.]
        noisy_qvm = api.QVMConnection(
            gate_noise=gate_noise, measurement_noise=meas_noise)    
        noisy_wavefunction, noisy_repetitions[i, j] = repeat_until_success(
            noisy_qvm.wavefunction, hhl, conditional_bit=flag)
        noisy_amplitudes = noisy_wavefunction.amplitudes
        print(noisy_wavefunction)
        fidelities[i, j] = fidelity(reference_amplitudes, noisy_amplitudes)
        if noise_type == 0:
            counter = 0
            fid = fidelity(reference_amplitudes, noisy_amplitudes)
            while (fid < 0.9) and (counter < 100):
                noisy_wavefunction, repetitions = repeat_until_success(
                        noisy_qvm.wavefunction, hhl, conditional_bit=flag)
                noisy_repetitions[i, j] += repetitions
                noisy_amplitudes = noisy_wavefunction.amplitudes
                fid = fidelity(reference_amplitudes, noisy_amplitudes)
                counter += 1
            if counter == 100:
                warnings.warn('No successful run achieved')
n = str(A.shape[0])
np.savetxt('output/' + noise_types[noise_type] + '_Fidelity_' + n + 'x' + n,
           fidelities)
np.savetxt('output/' + noise_types[noise_type] + '_Repetitions_' + n + 'x' + n,
           noisy_repetitions)

create_plot_fidelity('4', noise_types, noise_levels)
#create_plot_repetitions('4', noise_levels[0])
