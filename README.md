# Code for *[Bayesian Deep Learning on a Quantum Computer](https://link.springer.com/article/10.1007/s42484-019-00004-7)*
#### Zhikuan Zhao, Alejandro Pozas-Kerstjens, Patrick Rebentrost, and Peter Wittek

This is a repository for all code written for the article "*Bayesian Deep Learning on a Quantum Computer*. Zhikuan Zhao, Alejandro Pozas-Kerstjens, Patrick Rebentrost, and Peter Wittek. [Quantum Mach. Intell. 1, 41-51 (2019)](https://link.springer.com/article/10.1007/s42484-019-00004-7), [arXiv:1806.11463 [quant-ph]](https://arxiv.org/abs/1806.11463)."
It gives implementations of the [HHL algorithm](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.103.150502) in both pyQuil (for being run in Rigetti's architectures) and QASM (for IBM's architectures).

All code is written Python. Libraries required:
  - [qiskit](https://qiskit.org/) for the implementation in IBM
  - [pyquil](https://github.com/rigetticomputing/pyquil) and [grove](https://github.com/rigetticomputing/grove) for the implementation in Rigetti
  - matplotlib and [seaborn](https://seaborn.pydata.org) for plots
  - [tqdm](https://tqdm.github.io/) for progress bars
  - ast, math, numpy, os, scipy

##### Files included:
  - [HHL_IBMQ.py](https://gitlab.com/apozas/bayesian-dl-quantum/blob/master/HHL_IBMQ.py): Functions for implementing a 2x2 matrix inversion in the IBM's quantum computing software stack.
  - [HHL_Rigetti.py](https://gitlab.com/apozas/bayesian-dl-quantum/blob/master/HHL_Rigetti.py): Functions for implementing arbitrary-size matrix inversion in the Rigetti's quantum computing software stack.
  - [QPU_2x2_IBMQ.py](https://gitlab.com/apozas/bayesian-dl-quantum/blob/master/QPU_2x2_IBMQ.py): Runs the HHL algorithm on the IBM Quantum Experience QPU.
  - [QPU_2x2_Rigetti.py](https://gitlab.com/apozas/bayesian-dl-quantum/blob/master/QPU_2x2_Rigetti.py): Runs the HHL algorithm on Rigetti's QPU.
  - [ibmq_noise_benchmark.py](https://gitlab.com/apozas/bayesian-dl-quantum/blob/master/ibmq_noise_benchmark.py): Performs noisy classical simulations of the HHL algorithm in IBM's software stack.
  - [rigetti_noise_benchmark.py](https://gitlab.com/apozas/bayesian-dl-quantum/blob/master/rigetti_noise_benchmark.py): Performs noisy classical simulations of the HHL algorithm in Rigetti's software stack.

##### How to cite
If you use this code in your research, please cite it as follows:

Z. Zhao, A. Pozas-Kerstjens, P. Rebentrost, P. Wittek, "Bayesian Deep Learning on a Quantum Computer", (2018), arXiv:1806.11463 [quant-ph], https://arxiv.org/abs/1806.11463

BibTeX:
```
@article{Zhao2018BDLQC,
  title 		= {{Bayesian} deep learning on a quantum computer},
  author		= {Zhao, Zhikuan and Pozas-Kerstjens, Alejandro and Rebentrost, Patrick and Wittek, Peter},
  journal 	= {Quantum Mach. Intell.},
  year      = {2019},
  eprint		= {1806.11463},
  month 		= {may},
  pages     = {41--51},
  volume    = {1},
  issue     = {1--2},
  doi       = {10.1007/s42484-019-00004-7},
  publisher = {Springer International Publishing},
  url 			= {https://link.springer.com/article/10.1007/s42484-019-00004-7}
}
```
