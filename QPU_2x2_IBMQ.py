import os
from qiskit import execute, IBMQ
from HHL_IBMQ import get_hhl_2x2, get_psuccess

if __name__ == '__main__':
    if not os.path.exists("output"):
        os.mkdir("output")
    if not os.path.exists("figures"):
        os.mkdir("figures")
    QX_TOKEN = 'YOUR_TOKEN'
    IBMQ.enable_account(QX_TOKEN)
    qpu = IBMQ.get_backend('ibmqx5')
    hhl = get_hhl_2x2()
    result = execute(hhl, backend=qpu, shots=8192, max_credits=5)
    count_dict = result.result().get_counts()

    with open('output/IBMQX_QPU', 'w') as file:
        file.write(str(count_dict))
    print(get_psuccess('output/IBMQX_QPU'))
